var express = require('express');
var bcrypt = require('bcryptjs');
var Mongoose = require('mongoose');
const User = require('../../src/models/user');
var Mongoose = require('mongoose');
const upload  = require('../../controllers/storage.js');

var router = express.Router();

router.post('/api/signup',upload.single("upload"),function(req,res,next)
{
    console.log( req.body);
    if(!(req.body.Username&&req.body.Password&&req.body.Email&&req.body.Dob&&req.body.Phonenumber))
    {
        return res.send({Error:"Incomplete information"});
    }
    console.log(req.body);
    let hash = bcrypt.hashSync(req.body.Password,14);                                               //Encrypting The user passwords
    req.body.Password = hash;

    var user = new User({                                                                          //Creating a new User object     
        _id:new Mongoose.Types.ObjectId(),
        UserName:req.body.Username,
        Email:req.body.Email,
        PhoneNumber:req.body.Phonenumber,
        DOB:req.body.Dob,
        Password:req.body.Password,
        Profile:'/images/'+req.file.filename
    });

    user.save().then(user=>{
        console.log(user);
        console.log("help");
        res.status(200);

        return res.send({usercreated:true,token:user._id});
    }).catch(err=>{
     console.log(err);
    if(err!=null)                                                                                   //Handling errors
    {
        var errmsg = [];


        if(err.name === "ValidationError")
        {
            console.log(err.errors);
            for (field in err.errors) {
                
                errmsg.push( err.errors[field].message); 
            }

        }
        res.status(404);
        res.send({errors:errmsg});
    }
    

    });

});

module.exports = router;