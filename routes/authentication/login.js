var express = require('express');
const User = require('../../src/models/user');
var router = express.Router();
var bcrypt = require('bcryptjs');

router.post('/api/login',function(req,res)
{
  if(!req.body.Email){
      return res.send({err:"Enter a valid Email"});
  }
  if(!req.body.Password){
      return res.send({err:"Enter Password"});
  }
  User.findOne({Email:req.body.Email},(err,user)=>{
      if(err||!user||!bcrypt.compareSync(req.body.Password,user.Password)){
          res.status(404);
          return res.send({error:"Incorrect email/password."});
      }
      res.status(200);
      return res.send({Loggedin:true});
    }); 

});
module.exports = router;