var express = require('express');
const Card = require('../../src/models/Card');
var route = express.Router();

route.get('/cards/category/:category', (req, res) => {
    Card.find({category:req.params.category},(err, files) => {

        if(err){
            res.status(404);
            return res.send("An error occured");
        }
        res.status(200);
        return res.send(files);

    });

});
module.exports = route;