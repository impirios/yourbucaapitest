var express = require('express');
var router = express.Router();
const Grid = require('gridfs-stream');
const Mongoose = require('mongoose');

let conn = Mongoose.connection;
let gfs;
conn.once('open',()=>{
    gfs = Grid(conn.db,Mongoose.mongo);
    gfs.collection('images');
});//Creating file stream for images


//const gfs  = require('../../controllers/gfs.js');

route.get('/cards/:filename', (req, res) => {

    gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
        //check if files exist
        if (!file || file.length == 0) {
            return res.status(404).json({
                err: "No files exist"
            });
        }
        //check if the file is an image
        if (file.contentType === 'image/jpeg' || file.contentType === "image/png") {
            //read output to browser
            const readStream = gfs.createReadStream(file.filename);
            readStream.pipe(res)
        } else {
            res.status(404).json({
                err: "Not an image"
            });
        }
    });
});

module .exports = router;