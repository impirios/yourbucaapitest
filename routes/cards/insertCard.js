var express = require('express');
//initialising multer with the middleware 
var express = require('express');
const Card = require('../../src/models/Card');
var router = express.Router();
var Mongoose = require('mongoose');
const upload  = require('../../controllers/storage.js');



router.post('/api/uploadCard',upload.single("upload"),(req,res)=>{

    if(req.body.category!='Premium'&&req.body.category!='Basic')                  //throw an error if the category does not belong to Basic or premium 
    {
        return res.send({error:"Category "+req.body.category+" not defined"});
    }
    //new card object 
    var card = new Card({
        _id:new Mongoose.Types.ObjectId(),
        category:req.body.category,
        fileID:req.file.id,
        url:'/cards/'+req.file.filename
    });
    card.save().then(result=>{
        console.log(result);
        res.status(200);

        return res.send({cardadded:true});

    }).catch(err=>{
        console.log(err);
        res.status(404);
        return res.send({error:"Could not add the card"});
    });

});

module.exports = router;