var express = require('express');
var route = express.Router();
const Grid = require('gridfs-stream');
const Mongoose = require('mongoose');

let conn = Mongoose.connection;
let gfs;
conn.once('open',()=>{
    gfs = Grid(conn.db,Mongoose.mongo);
    gfs.collection('images');
});



route.get('/images/:filename', (req, res) => {
    gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
        //check if files exist
        if (!file || file.length == 0) {
            return res.status(404).json({
                err: "No files exist"
            });
        }
        //check if image
        if (file.contentType === 'image/jpeg' || file.contentType === "image/png") {
            //read output to browser
            const readStream = gfs.createReadStream(file.filename);
            readStream.pipe(res)
        } else {
            res.status(404).json({
                err: "Not an image"
            });
        }
    });
});

module.exports = route;