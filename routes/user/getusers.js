var express = require('express');
const User = require('../../src/models/user');
var router = express.Router();


router.get('/api/users',(req,res)=>{
    User.find({},(err,users)=>{
        if(err){
            res.status(404);
            res.send("An error occured");
            return;
        }
        res.send(users);
    });
});
module.exports = router;