var express = require('express');
const User = require('../../src/models/user');
var router = express.Router();


router.post('/api/linkusers',(req,res)=>{
    User.findById(req.body.id1,(err,user)=>{
        if(err||user===null){
            res.status(404);
            return res.send("User not found");
        }
        User.findById(req.body.id2,(err,user2)=>{
            if(err||user2===null){
                res.status(404);
                return res.send("User not found");
            }
            var profile = user2.Profile;
            console.log(profile);
            user.updateOne(
                {$push:{ContactList:profile}},
                {safe: true, upsert: true, new : true},
                (err,model)=>{
                    if(err)
                    {
                        res.status(404);
                        return res.send("User not found");
                    }
                }


            );
            var profile = user.Profile;

            user2.updateOne(
                {$push:{ContactList:profile}},
                {safe: true, upsert: true, new : true},
                (err,model)=>{
                    if(err)
                    {
                        res.status(404);
                        return res.send("User not found");
                    }
                    res.status(200);
                    return res.send({Userlinked:true});
        
                }


            );

        });

    });
});

module.exports = router;