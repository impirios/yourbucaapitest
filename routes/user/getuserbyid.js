var express = require('express');
const User = require('../../src/models/user');
var router = express.Router();


router.post('/api/user/',(req,res)=>{
    User.findById(req.body.id,(err,user)=>{
        if(err||user===null){
            res.status(404);
            return res.send("User not found");
        }
        res.status(200);
        return res.send(user);
    });

});

module.exports = router;