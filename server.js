var express = require('express');
var path = require('path');
var Mongoose = require('mongoose');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());

const options = {
    useNewUrlParser: true,
    useCreateIndex: true,
    autoIndex: true, //this is the code I added that solved it all
    keepAlive: true,
    poolSize: 10,
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    family: 4, // Use IPv4, skip trying IPv6
    useFindAndModify: false,
    useUnifiedTopology: true
};


const Mongouri = "mongo url";

Mongoose.connect(Mongouri,options);

const signup = require('./routes/authentication/signup.js');
const GetUsers = require('./routes/user/getusers.js');
const GetUsersById = require('./routes/user/getuserbyid');
const linkusers = require('./routes/user/linkusers.js');
const login = require('./routes/authentication/login.js');
const insertCard = require('./routes/cards/insertCard');
const getCard = require('./routes/cards/getCard');
const getCardByCategory = require('./routes/cards/getcardbycategory');
const getUserProfilePicture = require('./routes/user/getUserImage.js');

app.use(signup);
app.use(login);
app.use(GetUsers);
app.use(GetUsersById);
app.use(linkusers);
app.use(insertCard);
app.use(getCard);
app.use(getCardByCategory);
app.use(getUserProfilePicture);







//endpoints routes 
app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/api/signup',(req,res)=>{
    res.sendFile(path.join(__dirname+'/signup.html'));

});

app.get('/api/login',(req,res)=>{
    res.sendFile(path.join(__dirname+'/login.html'));

});

app.get('/api/uploadCard',(req,res)=>{
    res.sendFile(path.join(__dirname+'/newcard.html'));

});

//endpoint routes ends



app.listen(process.env.PORT||3000,()=>{
    console.log("Server started");
});
